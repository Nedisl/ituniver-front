import {Component, OnInit, TemplateRef} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {DataService} from './data.service';
import {RestApiService} from './rest-api.service';
import {BsModalRef, BsModalService} from 'ngx-bootstrap';
import {NgxSpinnerService} from 'ngx-spinner';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'ituniver-front';
  page: number;
  showRegInfo: boolean;
  instance: any = null;
  name: string;
  companyName: string;
  modalRef: BsModalRef;
  submitDisabled: boolean;
  email: string;
  password: string;
  showLogin: boolean;
  subscriptions: any = {data: {courses: []}};
  first_name: string;
  last_name: string;
  father_name: string;
  reg_email: string;
  reg_password: string;
  repeat_password: string;
  university_id: number;
  city_id: number;
  phone: string;

  isEmailValid: boolean;
  isPasswordMatch: boolean;
  isNameCorrect: boolean;
  isPhoneCorrect: boolean;
  isUniversityCorrect: boolean;
  isCityCorrect: boolean;
  allCorrect: boolean;

  constructor(private router: Router,
              public data: DataService,
              private rest: RestApiService,
              private modalService: BsModalService,
              private spinner: NgxSpinnerService) {
    this.page = 1;
    this.university_id = 1;
    this.city_id = 1;
    this.isEmailValid = true;
    this.isPasswordMatch = true;
    this.isNameCorrect = true;
    this.isPhoneCorrect = true;
    this.isUniversityCorrect = true;
    this.isCityCorrect = true;
    this.allCorrect = true;
    this.first_name = '';
    this.last_name  = '';
    this.father_name = '';
    this.reg_email = '';
    this.reg_password = '';
    this.repeat_password = '';
    this.phone = '';
  }

  isSubscriptionExists(id) {
    return this.subscriptions.data.courses.some(course => course.course_id === id);
  }

  async registerUser() {
    this.submitDisabled = true;
    // tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid =  re.test(String(this.reg_email).toLowerCase());
    this.isPasswordMatch = ((this.reg_password === this.repeat_password) && this.reg_password !== '');
    this.isNameCorrect = (this.first_name !== '' && this.last_name !== '' && this.father_name !== '');
    this.isPhoneCorrect = (this.phone.length === 10);
    this.isUniversityCorrect = (this.university_id !== 0);
    this.isCityCorrect = (this.city_id !== 0);
    this.submitDisabled = true;
    // tslint:disable-next-line:max-line-length
    this.allCorrect = this.isEmailValid && this.isPasswordMatch && this.isNameCorrect && this.isPhoneCorrect && this.isUniversityCorrect && this.isCityCorrect;

    if (this.allCorrect) {
      try {
        if (this.university_id !== 0 && this.city_id !== 0) {
          if (this.reg_password === this.repeat_password) {
            await this.spinner.show();
            await this
              .rest
              .signupListener({
                email: this.reg_email,
                password: this.reg_password,
                first_name: this.first_name,
                last_name: this.last_name,
                father_name: this.father_name,
                phone: this.phone,
                university_id: this.university_id,
                city_id: this.city_id
              }).then(async (res) => {
                await this
                  .data
                  .addToast('Вы успешно зарегистрированы', '', 'success');
                await this.spinner.hide();
                window.location.reload();
                this.data.showLogin = false;
                this.showRegInfo = false;
              });

          } else {
            this
              .data
              .addToast('Пароли не совпадают!', '', 'error');
          }
        } else {
          this
            .data
            .addToast('Выберите город и университет!', '', 'error');
        }
      } catch (error) {
        const message = error.error.meta.message;
        this
          .data
          .addToast(message, '', 'error');
      }
    }

    this.submitDisabled = false;
  }

  async login() {
    this.submitDisabled = true;
// tslint:disable-next-line:max-line-length
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.isEmailValid = re.test(String(this.email).toLowerCase());
    if (this.isEmailValid) {
      try {
        const res = await this
          .rest
          .loginListener({
            email: this.email,
            password: this.password,
          });
        if (res['meta'].success) {
          localStorage.setItem('listener_token', res['data'].token);
          this
            .data
            .addToast('Вы успешно авторизованы', '', 'success');
          this.data.isReg = true;
          if (this.data.subscribeId !== null) {
            this.subscriptions = await this.rest.getUserSubscriptions();
            if (!this.isSubscriptionExists(this.data.subscribeId)) {
              await this
                .rest
                .subscribeForCourse({
                  course_id: this.data.subscribeId
                }).then(async (result) => {
                  console.log(result);
                  this
                    .data
                    .addToast('Вы успешно подписаны на курс', '', 'success');
                  await this.router.navigate(['/course', this.data.subscribeId]);
                  this.data.subscribeId = null;
                });
            } else {
              this
                .data
                .addToast('Вы уже подписаны на этот курс', '', 'error');
            }
          }
          window.location.reload();
          this.data.showLogin = false;
          this.showRegInfo = false;
        } else {
          this
            .data
            .addToast('Неверно введен логин или пароль', '', 'error');
        }
      } catch (error) {
        this
          .data
          .addToast(error, '', 'error');
      }
    }
    this.submitDisabled = false;
  }
  stre() {
    console.log(this.data.showLogin);
  }

  logout() {
    this.data.clearProfile();
    this.showRegInfo = true;
    this
      .data
      .addToast('Вы успешно вышли из своего аккаунта!', '', 'success');
    window.location.reload();
  }

  async ngOnInit() {
    this.data.showLogin = false;
    this.isEmailValid = true;
    this.email = '';
    this.data.getListenerProfile();
    this.showRegInfo = true;
    if (localStorage.getItem('listener_token')) {
      this.showRegInfo = false;
    }
    this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        if ((<NavigationEnd>e).url.indexOf('about') !== -1) {
          this.page = 2;
        } else if ((<NavigationEnd>e).url.indexOf('contacts') !== -1) {
          this.page = 3;
        } else if ((<NavigationEnd>e).url.indexOf('course') !== -1) {
          this.page = 4;
          const id = (<NavigationEnd>e).url.split('/')[2];
          this.rest.getCourseInstanceById(+id).then(course => {
            console.log(course);
            this.instance = course;
            this.name = this.instance.data.course.name;
            this.companyName = this.instance.data.course.company.name;
          });
        } else {
          this.page = 1;
        }
      }
    });
  }
  register() {
    window.open('http://ituniver.vyatsu.ru/lk/#/register', '_self');
  }
  profile() {
    window.open('http://ituniver.vyatsu.ru/lk/#/login', '_self');
  }
  reset() {
    window.open('http://ituniver.vyatsu.ru/lk/#/reset', '_self');
  }
}
