import { Component, OnInit } from '@angular/core';
import {DataService} from '../data.service';
import {RestApiService} from '../rest-api.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  courses: any = null;
  newCourses: any = null;
  oldCourses: any = null;
  realCourses: any = null;
  courseLimit: number;

  // TODO: Убрать это
  instances: any = {data: {course_instances: []}};
  subscriptions: any = {data: {courses: []}};
  isReg: boolean;
  constructor (public data: DataService,
               private rest: RestApiService,
               private router: Router,
               private spinner: NgxSpinnerService) {
  }
  async ngOnInit() {
    this.courseLimit = 3;
    if (this.courses == null) {
      await this.spinner.show();
    }
    this.isReg = false;
    await this.data.getListenerProfile();

    if (localStorage.getItem('listener_token')) {
      this.instances = await this.rest.getListenerCourseInstances();
      this.subscriptions = await this.rest.getUserSubscriptions();
      this.isReg = true;
    }

    this.courses = await this.rest.getUniversityCourseInstances(1);
    this.realCourses = await this.courses.data.course_instances;

    this.realCourses.sort((a, b) => {
      const dateA = new Date(a.apply_end_date).getTime();
      const dateB = new Date(b.apply_end_date).getTime();
      return dateA < dateB ? 1 : -1;
    });

    this.realCourses = this.realCourses.slice(0, this.courseLimit);
    this.newCourses =  await this.realCourses.filter(function (course) {
      return moment(course.apply_end_date) >= moment();
    });
    this.oldCourses =  await this.realCourses.filter(function (course) {
      return moment(course.apply_end_date) < moment();
    });

    await this.spinner.hide();
    console.log(this.courses);
  }

  async loadNew() {
    this.courseLimit += 3;
    this.realCourses = await this.courses.data.course_instances;

    this.realCourses.sort((a, b) => {
      const dateA = new Date(a.apply_end_date).getTime();
      const dateB = new Date(b.apply_end_date).getTime();
      return dateA < dateB ? 1 : -1;
    });

    this.realCourses = this.realCourses.slice(0, this.courseLimit);
    this.newCourses =  await this.realCourses.filter(function (course) {
      return moment(course.apply_end_date) >= moment();
    });
    this.oldCourses =  await this.realCourses.filter(function (course) {
      return moment(course.apply_end_date) < moment();
    });
    console.log(this.oldCourses);
  }

  private getTime(date?: Date) {
    return date != null ? new Date(date).getTime() : 0;
  }



  async isInstanceExists(id) {
    return this.instances.data.course_instances.some(instance => instance.course_instance_id === id );
  }

  async isSubscriptionExists(id) {
    return this.subscriptions.data.courses.some(course => course.course_id === id );
  }

}
