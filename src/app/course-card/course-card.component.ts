import {Component, Input, OnInit} from '@angular/core';
import {RestApiService} from '../rest-api.service';
import {DataService} from '../data.service';
import {Router} from '@angular/router';
import * as moment from 'moment';

@Component({
  selector: 'app-course-card',
  templateUrl: './course-card.component.html',
  styleUrls: ['./course-card.component.scss']
})
export class CourseCardComponent implements OnInit {
  // tslint:disable-next-line:no-input-rename
  @Input('course')
  course: any;

  startDateDay: any;
  startDateMonth: any;
  endDateDay: any;
  endDateMonth: any;
  applyEndDateDay: any;
  applyEndDateMonth: any;
  picture: string;

  // tslint:disable-next-line:no-input-rename
  @Input('showbutton')
  showbutton: any;

  // tslint:disable-next-line:no-input-rename
  @Input('showsubscription')
  showsubscription: any;

  isApplyShowed: boolean;
  isSubscriptionShowed: boolean;
  constructor(private rest: RestApiService,
              public  data: DataService,
              private router: Router) { }

  ngOnInit() {
    this.picture = '';
    if (this.course.picture) {
      this.picture = this.course.picture.link;
    }
    this.showbutton.then(x => { this.isApplyShowed = !x; });
    this.showsubscription.then(x => { this.isSubscriptionShowed = !x; });
    this.startDateDay =   moment(this.course.start_date).subtract(1, 'days').format('D');
    console.log(this.course.start_date);
    this.startDateMonth =   this.getMonth(+moment(this.course.start_date).subtract(1, 'days').format('MM'));
    this.endDateDay =  moment(this.course.end_date).subtract(1, 'days').format('D');
    this.endDateMonth =   this.getMonth(+moment(this.course.end_date).subtract(1, 'days').format('MM'));
    this.applyEndDateDay = moment(this.course.apply_end_date).subtract(1, 'days').format('D');
    this.applyEndDateMonth = this.getMonth(+moment(this.course.apply_end_date).subtract(1, 'days').format('MM'));
    console.log(this.course);
  }

  login() {
    this
      .data
      .addToast('Для того чтобы подписаться на курс, необходимо войти', '', 'info');
  }


  getMonth(month) {
    switch (month) {
      case 1: return 'янв';
      case 2: return 'фев';
      case 3: return 'мар';
      case 4: return 'апр';
      case 5: return 'май';
      case 6: return 'июн';
      case 7: return 'июл';
      case 8: return 'авг';
      case 9: return 'сен';
      case 10: return 'окт';
      case 11: return 'ноя';
      case 12: return 'дек';
    }
    return 'янв';
  }

  async subscribeForCourse() {
    await this
      .rest
      .subscribeForCourse({
        course_id: this.course.course.id
      }).then((res) => {
        console.log(res);
        this
          .data
          .addToast('Вы успешно подписаны на курс', '', 'success');
      });
  }

  async signUpForCourse(instanceId) {
    await this
      .rest
      .signUpForCourse({
        course_instance_id: instanceId
      }).then((res) => {
        this
          .data
          .addToast('Вы успешно подали заявку на курс', '', 'success');
      });
  }

  isClosed() {
    if (this.course) {
      return moment() > moment(this.course.apply_end_date);
    } else {
      return true;
    }
  }

}
