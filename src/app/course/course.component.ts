import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RestApiService} from '../rest-api.service';
import {DataService} from '../data.service';
import * as moment from 'moment';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.scss']
})
export class CourseComponent implements OnInit {
  private sub: any;
  course: any;
  id: number;
  courseId: number;
  name: string;
  description: string;
  lecturer_photo: string;
  logo: string;
  lecturerPhoto: string;
  lecturerName: string;
  lecturerAbout: string;
  companyName: string;
  companyLogo: string;
  startDateDay: any;
  startDateMonth: any;
  endDateDay: any;
  endDateMonth: any;
  applyEndDateDay: any;
  applyEndDateMonth: any;
  phone: string;
  isReg: boolean;
  courseSignUpPage: number;
  listenerName: string;
  email: string;
  weeks: any;
  subscriptions: any = {data: {courses: []}};
  instances: any = {data: {course_instances: []}};
  constructor(private route: ActivatedRoute,
              private rest: RestApiService,
              public data: DataService,
              private spinner: NgxSpinnerService) { }

  async ngOnInit() {
    this.spinner.show();
    this.courseSignUpPage = 1;
    this.listenerName = '';
    this.phone = '';
    this.email = '';

    if (this.data.user) {
      this.listenerName = this.data.user.last_name + ' ' + this.data.user.first_name + ' ' + this.data.user.father_name;
      this.phone = this.data.user.phone;
      this.email = this.data.user.email;
    }

    this.sub = await this.route.params.subscribe(params => {
      this.id = +params['id'];
      this.rest.getCourseInstanceById(+params['id']).then(async course => {
        console.log(course);
        this.course = course;
        this.data.currentCourse = course;
        this.name = this.course.data.course.name;
        this.courseId = this.course.data.course.id;
        this.isReg = false;
        if (localStorage.getItem('listener_token')) {
          this.subscriptions = await this.rest.getUserSubscriptions();
          this.instances = await this.rest.getListenerCourseInstances();
          this.isReg = true;
        }
        this.description = this.course.data.course.description;
        this.logo = 'http://ituniver.vyatsu.ru/uploads/' + this.course.data.course.logo;
        this.lecturerPhoto = 'http://ituniver.vyatsu.ru/uploads/' + this.course.data.course.lecturer_photo;
        this.companyLogo = 'http://ituniver.vyatsu.ru/uploads/' + this.course.data.course.company.avatar;
        this.companyName = this.course.data.course.company.name;
        this.lecturerName =  this.course.data.course.lecturer_name;
        this.startDateDay =   moment(this.course.data.start_date).subtract(1, 'days').format('D');
        this.startDateMonth =   this.getMonth(+moment(this.course.data.start_date).subtract(1, 'days').format('MM'));
        this.endDateDay =  moment(this.course.data.end_date).subtract(1, 'days').format('D');
        this.endDateMonth =   this.getMonth(+moment(this.course.data.end_date).subtract(1, 'days').format('MM'));
        this.applyEndDateDay = moment(this.course.data.apply_end_date).subtract(1, 'days').format('D');
        this.applyEndDateMonth = this.getMonth(+moment(this.course.data.apply_end_date).subtract(1, 'days').format('MM'));
        this.lecturerAbout = this.course.data.course.lecturer_about;
        if (this.course.data.course.weeks) {
          this.weeks = JSON.parse(this.course.data.course.weeks).weeks;
        } else {
          this.weeks = [];
        }

        console.log(this.weeks);
        this.spinner.hide();
      });
    });
  }

  isClosed() {
    if (this.course) {
      return moment() > moment(this.course.data.apply_end_date);
    } else {
      return true;
    }
  }

  isSubscriptionExists(id) {
    return this.subscriptions.data.courses.some(course => course.course_id === id);
  }
  subs() {
    this.data.showLogin = true; this.data.subscribeId = this.courseId;
  }

  isInstanceExists(id) {
    return this.instances.data.course_instances.some(instance => instance.course_instance_id === id );
  }

  async signUpForCourse() {
    if (this.courseSignUpPage === 1) {
      this.courseSignUpPage++;
    } else if (this.courseSignUpPage === 2 && this.listenerName === '') {
      this
        .data
        .addToast('Введите ФИО', '', 'error');
    } else if (this.courseSignUpPage === 2) {
      this.courseSignUpPage++;
    } else if (this.courseSignUpPage === 3 && this.email === '') {
      this
        .data
        .addToast('Введите E-mail', '', 'error');
    } else if (this.courseSignUpPage === 3) {
      this.courseSignUpPage++;
    } else if (this.courseSignUpPage === 4 && this.phone === '') {
      this
        .data
        .addToast('Введите телефон', '', 'error');
    } else if (this.courseSignUpPage === 4) {
      this.courseSignUpPage++;
      if (localStorage.getItem('listener_token')) {
        await this
          .rest
          .signUpForCourse({
            course_instance_id: this.id
          }).then((res) => {
            this
              .data
              .addToast('Вы успешно подали заявку на курс', '', 'success');
          });
      } else {
        this.register();
      }
    }
  }

  getMonth(month) {
    switch (month) {
      case 1: return 'янв';
      case 2: return 'фев';
      case 3: return 'мар';
      case 4: return 'апр';
      case 5: return 'май';
      case 6: return 'июн';
      case 7: return 'июл';
      case 8: return 'авг';
      case 9: return 'сен';
      case 10: return 'окт';
      case 11: return 'ноя';
      case 12: return 'дек';
    }
    return 'янв';
  }

  async subscribeForCourse() {
    await this
      .rest
      .subscribeForCourse({
        course_id: this.courseId
      }).then((res) => {
        console.log(res);
        this
          .data
          .addToast('Вы успешно подписаны на курс', '', 'success');
      });
  }
  async phoneError() {
        this
          .data
          .addToast('Введите номер телефона', '', 'error');
  }

  login() {
    window.open('http://ituniver.vyatsu.ru/lk/#/login', '_self');
  }
  register() {
    window.open('http://ituniver.vyatsu.ru/lk/#/register', '_self');
  }
  profile() {
    window.open('http://ituniver.vyatsu.ru/lk/#/dashboard', '_self');
  }


}
